<?php
require_once "classe_mysql.php";
require_once 'vendor/autoload.php';
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

$host = getenv('MYSQL_HOST');
$user = getenv('MYSQL_USER');
$senha = getenv('MYSQL_PASSWORD');
$dbase = getenv('MYSQL_DATABASE');
$pathdir = getenv('REPFILES_DIRECTORY');
$iterator = new RecursiveDirectoryIterator($pathdir);

$mysql = new conexao($host,$user,$senha,$dbase);

foreach (new RecursiveIteratorIterator($iterator) as $entry) {
    if (strstr($entry->getFilename(), "rep")) {
        $filename = $entry;
        $fppath = $entry;
        $fp = fopen("$fppath", "r");

        if ($fp) {
            while (!feof($fp)) {
                $bf = fgets($fp, 4096);
            }
        }

        list(
            $local,
            $data,
            $hora,
            $tempint,
            $pressao_inst,
            $pressao_med,
            $pressao_max,
            $pressao_min,
            $temperatura_inst,
            $temperatura_med,
            $temperatura_max,
            $temperatura_min,
            $umidaderel_inst,
            $umidaderel_med,
            $umidaderel_max,
            $umidaderel_min,
            $radsolarglob_inst,
            $radsolarglob_med,
            $radsolarglob_max,
            $radsolarglob_min,
            $radsolarglob_soma,
            $radsolarliq_inst,
            $radsolarliq_med,
            $radsolarliq_max,
            $radsolarliq_min,
            $radsolarliq_soma,
            $dirvento_med_dez,
            $dirvento_max_dez,
            $dirvento_min_dez,
            $velvento_med_dez,
            $velvento_max_dez,
            $velvento_min_dez,
            $dirvento_med_hra,
            $dirvento_max_hra,
            $dirvento_min_hra,
            $velvento_med_hra,
            $velvento_max_hra,
            $velvento_min_hra,
            $precip,
            $bateria
        ) = explode(",", $bf);

    $local = trim($local);
    $data = trim($data);
    $hora = trim($hora);
    $tempint = trim($tempint, " \t\r\0\/");
    $pressao_inst = trim($pressao_inst, " \t\r\0\/");
    $pressao_med = trim($pressao_med, " \t\r\0\/");
    $pressao_max = trim($pressao_max, " \t\r\0\/");
    $pressao_min = trim($pressao_min, " \t\r\0\/");
    $temperatura_inst = trim($temperatura_inst, " \t\r\0\/");
    $temperatura_med = trim($temperatura_med, " \t\r\0\/");
    $temperatura_max = trim($temperatura_max, " \t\r\0\/");
    $temperatura_min = trim($temperatura_min, " \t\r\0\/");
    $umidaderel_inst = trim($umidaderel_inst, " \t\r\0\/");
    $umidaderel_med = trim($umidaderel_med, " \t\r\0\/");
    $umidaderel_max = trim($umidaderel_max, " \t\r\0\/");
    $umidaderel_min = trim($umidaderel_min, " \t\r\0\/");
    $radsolarglob_inst = trim($radsolarglob_inst, " \t\r\0\/");
    $radsolarglob_med = trim($radsolarglob_med, " \t\r\0\/");
    $radsolarglob_max = trim($radsolarglob_max, " \t\r\0\/");
    $radsolarglob_min = trim($radsolarglob_min, " \t\r\0\/");
    $radsolarglob_soma = trim($radsolarglob_soma, " \t\r\0\/");
    $radsolarliq_inst = trim($radsolarliq_inst, " \t\r\0\/");
    $radsolarliq_med = trim($radsolarliq_med, " \t\r\0\/");
    $radsolarliq_max = trim($radsolarliq_max, " \t\r\0\/");
    $radsolarliq_min = trim($radsolarliq_min, " \t\r\0\/");
    $radsolarliq_soma = trim($radsolarliq_soma, " \t\r\0\/");
    $dirvento_med_dez = trim($dirvento_med_dez, " \t\r\0\/");
    $dirvento_max_dez = trim($dirvento_max_dez, " \t\r\0\/");
    $dirvento_min_dez = trim($dirvento_min_dez, " \t\r\0\/");
    $velvento_med_dez = trim($velvento_med_dez, " \t\r\0\/");
    $velvento_max_dez = trim($velvento_max_dez, " \t\r\0\/");
    $velvento_min_dez = trim($velvento_min_dez, " \t\r\0\/");
    $dirvento_med_hra = trim($dirvento_med_hra, " \t\r\0\/");
    $dirvento_max_hra = trim($dirvento_max_hra, " \t\r\0\/");
    $dirvento_min_hra = trim($dirvento_min_hra, " \t\r\0\/");
    $velvento_med_hra = trim($velvento_med_hra, " \t\r\0\/");
    $velvento_max_hra = trim($velvento_max_hra, " \t\r\0\/");
    $velvento_min_hra = trim($velvento_min_hra, " \t\r\0\/");
    $precip = trim($precip, "\t\r\0\/");
    $bateria = trim($bateria, "\t\r\0\/");
    $tempint = $tempint!=="" ? $tempint : 'NULL';
    $pressao_inst = $pressao_inst!=="" ? $pressao_inst : 'NULL';
    $pressao_med = $pressao_med!=="" ? $pressao_med : 'NULL';
    $pressao_max = $pressao_max!=="" ? $pressao_max : 'NULL';
    $pressao_min = $pressao_min!=="" ? $pressao_min : 'NULL';
    $temperatura_inst = $temperatura_inst!=="" ? $temperatura_inst : 'NULL';
    $temperatura_med = $temperatura_med!=="" ? $temperatura_med : 'NULL';
    $temperatura_max = $temperatura_max!=="" ? $temperatura_max : 'NULL';
    $temperatura_min = $temperatura_min!=="" ? $temperatura_min : 'NULL';
    $umidaderel_inst = $umidaderel_inst!=="" ? $umidaderel_inst : 'NULL';
    $umidaderel_med = $umidaderel_med!=="" ? $umidaderel_med : 'NULL';
    $umidaderel_max = $umidaderel_max!=="" ? $umidaderel_max : 'NULL';
    $umidaderel_min = $umidaderel_min!=="" ? $umidaderel_min : 'NULL';
    $radsolarglob_inst = $radsolarglob_inst!=="" ? $radsolarglob_inst : 'NULL';
    $radsolarglob_med = $radsolarglob_med!=="" ? $radsolarglob_med : 'NULL';
    $radsolarglob_max = $radsolarglob_min!=="" ? $radsolarglob_max : 'NULL';
    $radsolarglob_min = $radsolarglob_min!=="" ? $radsolarglob_min : 'NULL';
    $radsolarglob_soma = $radsolarglob_soma!=="" ? $radsolarglob_soma : 'NULL';
    $radsolarliq_inst = $radsolarliq_inst!=="" ? $radsolarliq_inst : 'NULL';
    $radsolarliq_med = $radsolarliq_med!=="" ? $radsolarliq_med : 'NULL';
    $radsolarliq_max = $radsolarliq_max!=="" ? $radsolarliq_max : 'NULL';
    $radsolarliq_min = $radsolarliq_min!=="" ? $radsolarliq_min : 'NULL';
    $radsolarliq_soma = $radsolarliq_soma!=="" ? $radsolarliq_soma : 'NULL';
    $dirvento_med_dez = $dirvento_med_dez!=="" ? $dirvento_med_dez : 'NULL';
    $dirvento_max_dez = $dirvento_max_dez!=="" ? $dirvento_max_dez : 'NULL';
    $dirvento_min_dez = $dirvento_min_dez!=="" ? $dirvento_min_dez : 'NULL';
    $velvento_med_dez = $velvento_med_dez!=="" ? $velvento_med_dez : 'NULL';
    $velvento_max_dez = $velvento_max_dez!=="" ? $velvento_max_dez : 'NULL';
    $velvento_min_dez = $velvento_min_dez!=="" ? $velvento_min_dez : 'NULL';
    $dirvento_med_hra = $dirvento_med_hra!=="" ? $dirvento_med_hra : 'NULL';
    $dirvento_max_hra = $dirvento_max_hra!=="" ? $dirvento_max_hra : 'NULL';
    $dirvento_min_hra = $dirvento_min_hra!=="" ? $dirvento_min_hra : 'NULL';
    $velvento_med_hra = $velvento_med_hra!=="" ? $velvento_med_hra : 'NULL';
    $velvento_max_hra = $velvento_max_hra!=="" ? $velvento_max_hra : 'NULL';
    $velvento_min_hra = $velvento_min_hra!=="" ? $velvento_min_hra : 'NULL';
    $precip = $precip!=="" ? $precip : 'NULL';
    $bateria = $bateria!=="" ? $bateria : 'NULL';

        $sql_insert = "INSERT INTO tbcoleta VALUES(
            NULL,
            '$local',
            '$data',
            '$hora',
            '$tempint',
            '$pressao_inst',
            '$pressao_med',
            '$pressao_max',
            '$pressao_min',
            '$temperatura_inst',
            '$temperatura_med',
            '$temperatura_max',
            '$temperatura_min',
            '$umidaderel_inst',
            '$umidaderel_med',
            '$umidaderel_max',
            '$umidaderel_min',
            '$radsolarglob_inst',
            '$radsolarglob_med',
            '$radsolarglob_max',
            '$radsolarglob_min',
            '$radsolarglob_soma',
            '$radsolarliq_inst',
            '$radsolarliq_med',
            '$radsolarliq_max',
            '$radsolarliq_min',
            '$radsolarliq_soma',
            '$dirvento_med_dez',
            '$dirvento_max_dez',
            '$dirvento_min_dez',
            '$velvento_med_dez',
            '$velvento_max_dez',
            '$velvento_min_dez',
            '$dirvento_med_hra',
            '$dirvento_max_hra',
            '$dirvento_min_hra',
            '$velvento_med_hra',
            '$velvento_max_hra',
            '$velvento_min_hra',
            '$precip',
            '$bateria'
        )";

        $mysql->sql_query($sql_insert);

        $fileproc = str_replace(".rep", ".proc", $filename);
        rename($fppath, $fileproc);
    }
}
