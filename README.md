# rep2sql

# Introdução

Este repositório exporta dados de previsão meteorológica gerados pela execução do Modelo [WRF](http://www.wrf-model.org/index.php) para um Banco de Dados .

# Créditos

- Projeto original
  * Ulysses Monteiro
