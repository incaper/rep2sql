<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['txtdata'] != '')
{

$data = $_POST['txtdata'];
$local = $_POST['txtlocal'];

list($dia,$mes,$ano) = explode("/",$data);

$data = "$ano-$mes-$dia";

include("classe_mysql.php");

require_once 'vendor/autoload.php';
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

$host = getenv('MYSQL_HOST');
$user = getenv('MYSQL_USER');
$senha = getenv('MYSQL_PASSWORD');
$dbase = getenv('MYSQL_DATABASE');

$mysql = new conexao($host,$user,$senha,$dbase);

if ($local != '') $consulta = "select * from tbcoleta where data like '$data' and local like '$local'";
if ($local == '') $consulta = "select * from tbcoleta where data like '$data'";

$sql = $mysql->sql_query($consulta);


echo "<table border=1>\n";

foreach( $sql->fetchAll(PDO::FETCH_ASSOC) as $dado )
{

	$local = $dado['local'];
	$data = $dado['data'];
	$hora = $dado['hora'];
	$tempint = $dado['tempint'];
	$pressao_inst = $dado['pressao_inst'];
	$pressao_med = $dado['pressao_med'];
	$pressao_max = $dado['pressao_max'];
	$pressao_min = $dado['pressao_min'];
	$temperatura_inst = $dado['temperatura_inst'];
	$temperatura_med = $dado['temperatura_med'];
	$temperatura_max = $dado['temperatura_max'];
	$temperatura_min = $dado['temperatura_min'];
	$umidaderel_inst = $dado['umidaderel_inst'];
	$umidaderel_med = $dado['umidaderel_med'];
	$umidaderel_max = $dado['umidaderel_max'];
	$umidaderel_min = $dado['umidaderel_min'];
	$radsolarglob_inst = $dado['radsolarglob_inst'];
	$radsolarglob_med = $dado['radsolarglob_med'];
	$radsolarglob_max = $dado['radsolarglob_max'];
	$radsolarglob_min = $dado['radsolarglob_min'];
	$radsolarglob_soma = $dado['radsolarglob_soma'];
	$radsolarliq_inst = $dado['radsolarliq_inst'];
	$radsolarliq_med = $dado['radsolarliq_med'];
	$radsolarliq_max = $dado['radsolarliq_max'];
	$radsolarliq_min = $dado['radsolarliq_min'];
	$radsolarliq_soma = $dado['radsolarliq_soma'];
	$dirvento_med_dez = $dado['dirvento_med_dez'];
	$dirvento_max_dez = $dado['dirvento_max_dez'];
	$dirvento_min_dez = $dado['dirvento_min_dez'];
	$velvento_med_dez = $dado['velvento_med_dez'];
	$velvento_max_dez = $dado['velvento_max_dez'];
	$velvento_min_dez = $dado['velvento_min_dez'];
	$dirvento_med_hra = $dado['dirvento_med_hra'];
	$dirvento_max_hra = $dado['dirvento_max_hra'];
	$dirvento_min_hra = $dado['dirvento_min_hra'];
	$velvento_med_hra = $dado['velvento_med_hra'];
	$velvento_max_hra = $dado['velvento_max_hra'];
	$velvento_min_hra = $dado['velvento_min_hra'];
	$precip = $dado['precip'];
	$bateria = $dado['bateria'];


if ( $local != '' )
{
	echo "<tr>\n";
	echo "<td>$local</td>\n";
	echo "<td>$data</td>\n";
	echo "<td>$hora</td>\n";
	echo "<td>$tempint</td>\n";
	echo "<td>$pressao_inst</td>\n";
	echo "<td>$pressao_med</td>\n";
	echo "<td>$pressao_max</td>\n";
	echo "<td>$pressao_min</td>\n";
	echo "<td>$temperatura_inst</td>\n";
	echo "<td>$temperatura_med</td>\n";
	echo "<td>$temperatura_max</td>\n";
	echo "<td>$temperatura_min</td>\n";
	echo "<td>$umidaderel_inst</td>\n";
	echo "<td>$umidaderel_med</td>\n";
	echo "<td>$umidaderel_max</td>\n";
	echo "<td>$umidaderel_min</td>\n";
	echo "<td>$radsolarglob_inst</td>\n";
	echo "<td>$radsolarglob_med</td>\n";
	echo "<td>$radsolarglob_max</td>\n";
	echo "<td>$radsolarglob_min</td>\n";
	echo "<td>$radsolarliq_inst</td>\n";
	echo "<td>$radsolarliq_med</td>\n";
	echo "<td>$radsolarliq_max</td>\n";
	echo "<td>$radsolarliq_min</td>\n";
	echo "<td>$dirvento_med_dez</td>\n";
	echo "<td>$dirvento_max_dez</td>\n";
	echo "<td>$dirvento_min_dez</td>\n";
	echo "<td>$velvento_med_dez</td>\n";
	echo "<td>$velvento_max_dez</td>\n";
	echo "<td>$velvento_min_dez</td>\n";
	echo "<td>$dirvento_med_hra</td>\n";
	echo "<td>$dirvento_max_hra</td>\n";
	echo "<td>$dirvento_min_hra</td>\n";
	echo "<td>$velvento_med_hra</td>\n";
	echo "<td>$velvento_max_hra</td>\n";
	echo "<td>$velvento_min_hra</td>\n";
	echo "<td>$precip</td>\n";
	echo "<td>$bateria</td>\n";
	echo "</tr>\n";
} // fecha if


}


echo "</table>\n";
$sql = null;
} // fecha requestmethod

?>
