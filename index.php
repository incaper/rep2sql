<?php

require_once 'vendor/autoload.php';
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

$usuario = getenv('HTTP_USER');
$senha = getenv('HTTP_PASSWORD');

  if (!isset($_SERVER['PHP_AUTH_USER'])) {
    header('WWW-Authenticate: Basic realm="ACESSO RESTRITO"');
    header('HTTP/1.0 401 Unauthorized');

    exit;
  } elseif ( $_SERVER['PHP_AUTH_USER'] == $usuario && $_SERVER['PHP_AUTH_PW'] == $senha){
	include('busca.php');
 }
?>
