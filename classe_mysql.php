<?php
class conexao {

    var $host; // Nome ou IP do Servidor
    var $user; // Usu�rio do Servidor MySQL
    var $senha; // Senha do Usu�rio MySQL
    var $dbase; // Nome do seu Banco de Dados

    var $query;
    var $link;
    var $resultado;

    public function __construct($host,$user,$senha,$dbase) {
        $this->host = $host;
        $this->user = $user;
        $this->senha = $senha;
        $this->dbase = $dbase;
    }

    function MySQL(){

    }


    function conecta(){
        try {
        $this->link = new PDO(
            sprintf("mysql:host=%s;dbname=%s;charset=utf8mb4", $this->host, $this->dbase),
            $this->user,
            $this->senha
        );
        }
        catch(PDOException $exception) {
            print "Ocorreu um Erro na conex�o MySQL:";
      print "<b>".$exception->getMessage()."</b>";
      die();
        }
    }

    function sql_query($query){
        $this->conecta();
        $this->query = $query;

    try {
        if($this->resultado = $this->link->query($this->query)){
            $this->desconecta();
            return $this->resultado;
        }
    }
    catch(PDOException $exception) {
            print "Ocorreu um erro ao executar a Query MySQL: <b>$query</b>";
      print "<br><br>";
      print "Erro no MySQL: <b>".$exception->getMessage()."</b>";
            $this->desconecta();
      die();
        }
    }

    function desconecta(){
        return $this->link = null;
    }
}
?>

